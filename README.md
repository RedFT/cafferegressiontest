Testing Caffe Univariate Regression

"Perhaps we can try to solve a univariate regression problem with Caffe. For example, number of hours of studying vs test grade.

We don't need real world data for this test. We can generate x, y pairs from a linear equation.

Then, we can feed the data into Caffe and see if the loss value goes down.

Then, test the model with a random x value and see if caffe can output a number close to the expected y value."
