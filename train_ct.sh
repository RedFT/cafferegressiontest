#!/usr/bin/env bash

OUT_NAME=training_output_$(date +%d-%m-%y_%H-%M).txt

ARG_STRING=""
while (( "$#" )); do
  ARG_STRING="$TEST_STRING $1"
  shift
done

#$CAFFE_ROOT/build/tools/caffe train \
caffe train \
	-sigint_effect stop \
	-sighup_effect stop \
	-solver ct_solver.prototxt \
	$ARG_STRING \
	2> $OUT_NAME
