#!/usr/bin/env python2.7
from sys import argv
import random

from matplotlib import pyplot as plt
import numpy as np

usage = "USAGE: " + argv[0] + " [slope] [y-intercept]"
if len(argv) > 3:
    print "Not enough arguments"
    print usage
    exit(1)

MAX_STUDY_HOURS = 100
random.seed()
args = map(float, argv[1:])
slope = args[0]
y_intercept = args[1]

print "Slope:", slope
print "Y-Intercept:", y_intercept

def get_grade(hours_studied, sigma=None):
    global y_intercept
    global slope
    grade = y_intercept + slope * hours_studied
    if sigma:
        grade += sigma
    return grade


#hours_studied = random.uniform(0, MAX_STUDY_HOURS)

sigmas = []
hours_studied_vals = np.arange(0, 100, 0.01);
for i in range(0, 10000):
    sigma = random.uniform(-2.5, 2.5)
    sigmas.append(sigma)

grade_vals = map(get_grade, hours_studied_vals, sigmas)

hours_of_study_and_grade_pairs = zip(hours_studied_vals, grade_vals)

training_sample = random.sample(hours_of_study_and_grade_pairs,
        len(hours_of_study_and_grade_pairs)/3)
validation_sample = random.sample(hours_of_study_and_grade_pairs,
        len(hours_of_study_and_grade_pairs)/3)
testing_sample = random.sample(hours_of_study_and_grade_pairs,
        len(hours_of_study_and_grade_pairs)/3)

training_filename   = "train.txt"
validation_filename = "val.txt"
testing_filename    = "test.txt"

with open(training_filename, "w") as f:
    for hours, grade in training_sample:
        f.write(str(hours) + " " + str(grade) + "\n")

with open(validation_filename, "w") as f:
    for hours, grade in validation_sample:
        f.write(str(hours) + " " + str(grade) + "\n")

with open(testing_filename, "w") as f:
    for hours, grade in testing_sample:
        f.write(str(hours) + " " + str(grade) + "\n")



'''
test_vals = np.arange(0, 100, 1);
plt.plot(test_vals, get_grade(test_vals))
plt.grid(1)
plt.legend("Grade")
plt.show()
'''
