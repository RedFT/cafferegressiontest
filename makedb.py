#!/usr/bin/env python2.7

import h5py
import os
import shutil
import numpy as np
import caffe

TRAIN_DB='train.h5'
VAL_DB='val.h5'
TEST_DB='test.h5'

RESIZE=False
if RESIZE:
    RESIZE_HEIGHT=72
    RESIZE_WIDTH=72
else:
  RESIZE_HEIGHT=0
  RESIZE_WIDTH=0

'''
if os.path.exists(TRAIN_DB):
    shutil.rmtree(TRAIN_DB)
if os.path.exists(VAL_DB):
    shutil.rmtree(VAL_DB)
'''

f = open('train.txt', 'r') # training file
training_data = f.readlines()
f.close()

f = open('val.txt', 'r') # validation file
val_data = f.readlines()
f.close()

f = open('test.txt', 'r') # testing file
test_data = f.readlines()
f.close()

def makedb(name, lines):
    X = np.zeros((len(lines), 1, 1, 1), dtype='f4')
    Y = np.zeros(len(lines), dtype='f4')

    for i, line in enumerate(lines):
        hours_studied, grade = map(float, line.split())

        X[i, 0, 0, 0] = hours_studied
        Y[i] = grade

    with h5py.File(name + '.h5','w') as H:
        H.create_dataset( 'data', data=X ) # note the name X given to the dataset!
        H.create_dataset( 'label', data=Y ) # note the name y given to the dataset!

    with open(name + '_h5_list.txt','w') as L:
        L.write( name + '.h5' ) # list all h5 files you are going to use


makedb('train', training_data)
makedb('val', val_data)
makedb('test', test_data)
